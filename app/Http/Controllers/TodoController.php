<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// feb192020
// stablish connection between controller (todo controller) and app (todo.php)
use \App\Todo;
use \App\Category;
// feb192020


class TodoController extends Controller
{
	// feb192020
	// instead of index i used controller to show it is pikachu
	// Todo is from app Todo.php
     public function controller(){
     	// ::all has many friends like SELECT * in sql
     	$tasks = Todo::all(); 
     	// dd is for laravel means dump and die like var_dump(); and die(); of PHP
     	// dd($tasks);

     	// to pass a data or specific data use compact()
     	//tasks in view is the tasks.blade.php then tasks in compact is the $tasks variable
    	return view('tasks', compact('tasks'));
    }
    // Todo is from app Category.php
    public function create(){
    	$categories = Category::all();

    	return view('add-task', compact('categories'));
    }
    // if data is from from us Request then the storage
	public function store(Request $req){
		// dd($req);
		// error 419 is shown need @crsf below the form
		// new is then Todo is the model
		$new_task = new Todo;
		// the first title in the data base, the title in req is user generated in the form add-task.blade.php
		$new_task->title = $req->title;
		$new_task->body = $req->body;
		$new_task->category_id = $req->category_id;
		$new_task->status_id = 1;
		$new_task->user_id = 1;
		$new_task->save();

		return redirect('/tasks');
	}
	// this from task.blade for delete a href with todocontroller@destroy
	public function destroy($id){
		// find the data to delete
		// delete
		$taskToDelete = Todo::find($id);
		// dd($taskToDelete);
		$taskToDelete->delete();
		return redirect('/tasks');

	}

	public function markasDone($id){
		// find the task to update
		//update
		$taskToUpdate = Todo::find($id);
		if($taskToUpdate->status_id == 3){
			$taskToUpdate->status_id =1;
		}else{
			$taskToUpdate->status_id=3;
		}

		$taskToUpdate->save();
		return redirect('/tasks');
	}

    // end of feb192020
}
