<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//feb192020
// this is for connection to different app (Category $ Status)
use \App\Category;
use \App\Status;
// this is model
//feb192020


class Todo extends Model
{
    //feb 19,2020
    //here we set up the relationship and publish and return the data/details
	public function category(){
		return $this->belongsTo("\App\Category");
	}
	public function status(){
		// belongsTo and hasMany
		return $this->belongsTo("\App\Status");
	}
	// store is from from web.ph $req is starage it is pikachu

    //feb 19,2020
}
