<?php

namespace App\Providers;
// feb 18,2020
use Schema;


use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // feb 18, 2020
        Schema::defaultStringLength(191);
    }
}
