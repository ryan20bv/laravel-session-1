<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/profile', function () {
    return view('profile');
});

Route::get('/blade1', function () {
    return view('blade1');
});

Route::get('/blade2', function () {
    return view('blade2');
});

Route::get('/blade3', function () {
    return view('blade3');
});


// accessing welcome
// Controller is from Controller.php class Controller
Route::get('/welcome', 'Controller@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// feb192020
// views --to-- route -- to -- controller
// @controller is a function and a pickachu from controller 
// /addtask to access in browser / tasks as url
Route::get('/tasks','TodoController@controller');
Route::get('/addtask','TodoController@create');
// to save the new route from add button in add-task.blade.php
Route::post('/addtask', 'TodoController@store');
// id is variable/pikachu from just an storage from deletetask button
Route::delete('/deletetask/{id}', 'TodoController@destroy');
Route::patch('/markasdone/{id}', 'TodoController@markasDone');
// end of feb192020