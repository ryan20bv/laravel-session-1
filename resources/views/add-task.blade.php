@extends('layouts.app');
@section('content');
	<h1 class="text-center py5">ADD TASK </h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="/addtask" method="POST">
			{{-- this is for protection of data error 416 --}}
			@csrf
			{{-- this is for protection of data error 416 --}}
			<input type="text" name="title" class="form-control" placeholder="Enter Task Title">
			<input type="text" name="body" class="form-control" placeholder="Enter Task Body">
			<div class="form-group">
				<label for="category_id">Category:</label>
				<select name="category_id" class="form-control">
					@foreach($categories as $indiv_category)
					<option value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
					@endforeach
				</select>
			</div>
			{{-- send to route to process  --}}
			<button class="btn btn-info" type="submit">Add Task</button>
		</form>
		
	</div>
@endsection