@extends("layouts.app")
@section("content")

	<h1 class="text-center py-5">TODO LIST from tasks.blade.php</h1>
{{-- feb 19 2020 --}}
	<div class="row">
		{{-- @ symbol is replacement for opening php tag and @end is for closing tag--}}
		@foreach($tasks as $indiv_task)
			<div class="col-lg-3 my-2">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">
							{{-- insert php here use {{echo}} as echo--}}
							{{-- -> is replacement for [] --}}
							{{$indiv_task->title}}
						</h4>
						<p class="card-text">{{$indiv_task->body}}</p>
						{{-- <p class="card-text">{{$indiv_task->category_id}}</p> --}}
						{{-- after setting up the relationship --}}
						
						<p class="card-text">{{$indiv_task->category->name}}</p>
						{{-- <p class="card-text">{{$indiv_task->status_id}}</p> --}}
						<p class="card-text">{{$indiv_task->status->name}}</p>
					</div>
					<div class="card-footer">
						{{-- <a href="/deletetask/{{$indiv_task->id}}" class="btn btn-danger">Delete Task</a> --}}
						<form action="/deletetask/{{$indiv_task->id}}" method="POST">
							@csrf
							{{-- fro delete put and patch --}}
							@method('DELETE')
							{{-- fro delete put and patch --}}
							<button class="btn btn-danger " type="submit"> Delete Task</button>
						</form>
						<form action="/markasdone/{{$indiv_task->id}}" method="POST">
							@csrf
							@method('PATCH')
							<button class="btn btn-success" type="submit">
								@if($indiv_task->status_id !=1)
									<span>Mark as Pending</span>
								@else
									<span>Mark as Done</span>	
								@endif
							</button>
						</form>
					</div>
				</div>
			</div>
		{{-- @end is for closing php tag --}}
		@endforeach
	
	
</div>

{{-- feb 19 2020 --}}


@endsection